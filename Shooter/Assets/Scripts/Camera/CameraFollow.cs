﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float smoothing = 5f;

    // the vector between the camera and the target
    Vector3 offset;

    private void Start()
    {
        offset = transform.position - target.position;
    }

    private void FixedUpdate()
    {
        Vector3 newCameraPos = target.position + offset;
        // Allows to smootly move the camera
        transform.position = Vector3.Lerp(transform.position, newCameraPos, smoothing * Time.deltaTime);
    }

}
