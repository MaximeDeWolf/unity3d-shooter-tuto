﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class GameOverManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public float restartDelay = 5f;


    Animator anim;
    float restartTimer;


    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        if (playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger("GameOver");
            restartTimer += Time.deltaTime;
        }
        /*
         * Befaore, the game was retart by an event in the player death animation but we now
         * use a timer to know when the current level neads to be reload. It allows use to easily
         * control the timer before we reload the level
        */
         if(restartTimer >= restartDelay)
            {
                // .. then reload the currently loaded level.
                //Application.LoadLevel(Application.loadedLevel);
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            
            }

    }
}
