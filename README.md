# Unity3D-Shooter-Tuto



This game has been made by following this tutorial: https://unity3d.com/fr/learn/tutorials/s/survival-shooter-tutorial.

We used the *2018.3.12f1 Unity version* to make it. The assets pack we used is totally and can be download
here:
 https://assetstore.unity.com/packages/essentials/tutorial-projects/survival-shooter-tutorial-legacy-40756. This game is
 a pretty simple
 _third person shooter_ where the player has to avoid some horrible zombie toys to survive. He can
kill them with his toy riffle.



## Features


Here is all the features we use to make during _this_ tutorial with *Unity*:


1. Manage the animations for the player and the enemies
2. Use simple *Unity AI* to make enemies follow the player
3. Trigger some function in the enemies animation to make them disappear when they are dead
4. Create prefabs for the enemies and make them spawn at some location
5. Create some effects for the player shoot including particules and lights
6. Make an animation for the game over screen